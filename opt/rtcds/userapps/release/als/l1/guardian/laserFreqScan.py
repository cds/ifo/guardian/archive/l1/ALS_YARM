import numpy as np
import time

class LaserFreqScan(object):

    def __init__(self,ezca,startFreq,stopFreq,nSteps,crystalFreqChan,beatAmplChan,beatFreqChan,tStep=3):
        self._ezca = ezca
        self._startFreq = startFreq
        self._stopFreq = stopFreq
        self._nSteps = nSteps
        self._crystalFreqChan = crystalFreqChan
        self._beatAmplChan = beatAmplChan
        self._beatFreqChan = beatFreqChan
        self._tStep = tStep
        ezca[crystalFreqChan] = startFreq
        time.sleep(5)

        #Make an array for crystal frequency
        self._crystalFreqArray = np.linspace(startFreq,stopFreq,num=nSteps)
        #Initialize beat amplitude and frequency arrays
        self._beatAmplArray = np.zeros(nSteps)
        self._beatFreqArray = np.zeros(nSteps)

        self._counter = 0

    def scan(self):

        if self._counter < self._nSteps:
            self._ezca[self._crystalFreqChan] = self._crystalFreqArray[self._counter]
            time.sleep(self._tStep)
            self._beatAmplArray[self._counter] = self._ezca[self._beatAmplChan]
            self._beatFreqArray[self._counter] = self._ezca[self._beatFreqChan]
            self._counter += 1
            return False, None, None, None
        else:
            return True, self._crystalFreqArray, self._beatAmplArray, self._beatFreqArray

